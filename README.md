# Entgra Device Management Core

<a href='https://opensource.org/licenses/Apache-2.0'><img src='https://img.shields.io/badge/License-Apache%202.0-blue.svg'></a><br/>

[![pipeline status](https://gitlab.com/entgra/carbon-device-mgt/badges/master/pipeline.svg)](https://gitlab.com/entgra/carbon-device-mgt/commits/master)