<?xml version="1.0" encoding="utf-8"?>
<!--
~ Copyright (c) 2019, Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
~
~ Entgra (pvt) Ltd. licenses this file to you under the Apache License,
~ Version 2.0 (the "License"); you may not use this file except
~ in compliance with the License.
~ You may obtain a copy of the License at
~
~    http://www.apache.org/licenses/LICENSE-2.0
~
~ Unless required by applicable law or agreed to in writing,
~ software distributed under the License is distributed on an
~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
~ KIND, either express or implied.  See the License for the
~ specific language governing permissions and limitations
~ under the License.
-->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <parent>
        <groupId>org.wso2.carbon.devicemgt</groupId>
        <artifactId>application-mgt-feature</artifactId>
        <version>5.0.12-SNAPSHOT</version>
        <relativePath>../pom.xml</relativePath>
    </parent>

    <modelVersion>4.0.0</modelVersion>
    <artifactId>io.entgra.application.mgt.server.feature</artifactId>
    <packaging>pom</packaging>

    <name>Entgra - Application Management Server Feature</name>
    <url>https://entgra.io</url>
    <description>This feature contains the core bundles required for Back-end Application Management functionality
    </description>

    <dependencies>
        <dependency>
            <groupId>org.wso2.carbon.devicemgt</groupId>
            <artifactId>io.entgra.application.mgt.common</artifactId>
        </dependency>
        <dependency>
            <groupId>org.wso2.carbon.devicemgt</groupId>
            <artifactId>io.entgra.application.mgt.core</artifactId>
        </dependency>
        <dependency>
            <groupId>com.googlecode.plist</groupId>
            <artifactId>dd-plist</artifactId>
            <version>${googlecode.plist.version}</version>
        </dependency>
        <dependency>
            <groupId>commons-validator</groupId>
            <artifactId>commons-validator</artifactId>
            <version>${commons-validator.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.cxf</groupId>
            <artifactId>cxf-rt-frontend-jaxrs</artifactId>
            <version>${cxf.version}</version>
        </dependency>
        <dependency>
            <groupId>com.h2database.wso2</groupId>
            <artifactId>h2-database-engine</artifactId>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <artifactId>maven-resources-plugin</artifactId>
                <version>2.6</version>
                <executions>
                    <execution>
                        <id>copy-resources</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>src/main/resources</outputDirectory>
                            <resources>
                                <resource>
                                    <directory>resources</directory>
                                    <includes>
                                        <include>build.properties</include>
                                        <include>p2.inf</include>
                                    </includes>
                                </resource>
                            </resources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-antrun-plugin</artifactId>
                <executions>
                    <execution>
                        <id>create-app-mgt-schema</id>
                        <phase>package</phase>
                        <goals>
                            <goal>run</goal>
                        </goals>
                        <configuration>
                            <tasks>
                                <echo message="########### Create Application Management H2 Schema ###########" />
                                <property name="db.dir" value="target/maven-shared-archive-resources/database" />
                                <property name="userid" value="wso2carbon" />
                                <property name="password" value="wso2carbon" />
                                <property name="dbURL" value="jdbc:h2:file:${basedir}/${db.dir}/WSO2DM_APPM_DB;DB_CLOSE_ON_EXIT=FALSE" />

                                <mkdir dir="${basedir}/${db.dir}" />

                                <sql driver="org.h2.Driver" url="${dbURL}" userid="${userid}" password="${password}" autocommit="true" onerror="continue">
                                    <classpath refid="maven.dependency.classpath" />
                                    <classpath refid="maven.compile.classpath" />
                                    <classpath refid="maven.runtime.classpath" />

                                    <fileset file="${basedir}/src/main/resources/dbscripts/cdm/application-mgt/h2.sql" />
                                </sql>
                                <echo message="##################### END ####################" />
                            </tasks>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.wso2.maven</groupId>
                <artifactId>carbon-p2-plugin</artifactId>
                <version>${carbon.p2.plugin.version}</version>
                <executions>
                    <execution>
                        <id>p2-feature-generation</id>
                        <phase>package</phase>
                        <goals>
                            <goal>p2-feature-gen</goal>
                        </goals>
                        <configuration>
                            <id>io.entgra.application.mgt.server</id>
                            <propertiesFile>../../../features/etc/feature.properties</propertiesFile>
                            <adviceFile>
                                <properties>
                                    <propertyDef>org.wso2.carbon.p2.category.type:server</propertyDef>
                                    <propertyDef>org.eclipse.equinox.p2.type.group:false</propertyDef>
                                </properties>
                            </adviceFile>
                            <bundles>
                                <bundleDef>
                                    org.wso2.carbon.devicemgt:io.entgra.application.mgt.common:${carbon.device.mgt.version}
                                </bundleDef>
                                <bundleDef>
                                    org.wso2.carbon.devicemgt:io.entgra.application.mgt.core:${carbon.device.mgt.version}
                                </bundleDef>
                                <bundleDef>
                                    com.googlecode.plist:dd-plist:${googlecode.plist.version}
                                </bundleDef>
                                <bundleDef>
                                    commons-validator:commons-validator:${commons-validator.version}
                                </bundleDef>
                            </bundles>
                            <importBundles>
                            </importBundles>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
    <properties>
        <commons-validator.version>1.6</commons-validator.version>
    </properties>
</project>
