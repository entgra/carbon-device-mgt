package io.entgra.transport.mgt.sms.handler.common;

/**
 * This class holds SMS Management Constants
 */
public final class SMSHandlerConstants {

    public static final String SCOPE = "scope";
    public static final String SMS_CONFIG_XML_NAME = "sms-config.xml";
}
